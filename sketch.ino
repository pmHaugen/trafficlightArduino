void setup() {
  // put your setup code here, to run once:
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);

  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  
  Serial.begin(9600);

 digitalWrite(A0, HIGH);
 digitalWrite(A4, HIGH);
}

void loop() {
  //First On
  digitalWrite(2, LOW);
  digitalWrite(3, HIGH);
  digitalWrite(11, HIGH);
  delay(1000);

  digitalWrite(12, LOW);
  digitalWrite(11, LOW);
  digitalWrite(10, HIGH);
  delay(5000);

  //First Off
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  delay(1000);
  digitalWrite(11, LOW);
  digitalWrite(12, HIGH);
  delay(1000);
  digitalWrite(12, HIGH);
  digitalWrite(3, LOW);
  digitalWrite(2, HIGH);

  //Second on:
  digitalWrite(A4, LOW);
  digitalWrite(A3, HIGH);
  digitalWrite(A0, HIGH);
  digitalWrite(A1, HIGH);
  delay(1000);
  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(A2, HIGH);
  delay(5000);

  //Second off:
  digitalWrite(A2, LOW);
  digitalWrite(A1, HIGH);
  delay(1000);
  digitalWrite(A1, LOW);
  digitalWrite(A0, HIGH);
  delay(1000);
  digitalWrite(A3, LOW);
  digitalWrite(A4, HIGH);

  Serial.println(("Cycle Done"));
}
